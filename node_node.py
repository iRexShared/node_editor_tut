from node_graphics_node import QDMGraphicsNode


class Node():
    def __init__(self, scene, title='Some node'):

        self.scene = scene

        self.title = title

        self.graphics_node = QDMGraphicsNode(self, self.title)

        self.scene.add_node(self)
        self.scene.graphics_scene.addItem(self.graphics_node)

        self.inputs = []

        self.outputs = []
