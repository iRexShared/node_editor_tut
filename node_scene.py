from node_graphics_scene import QDMGraphicsScene


class Scene():
    def __init__(self):

        self.nodes = []
        self.lines = []
        self.graphics_scene = None

        self.scene_width = 70000
        self.scene_height = 70000

        self.init_ui()

    def init_ui(self):
        self.graphics_scene = QDMGraphicsScene(self)
        self.graphics_scene.set_graphics_scene(self.scene_width, self.scene_height)

    def add_node(self, node):
        self.nodes.append(node)

    def remove_node(self, node):
        self.nodes.remove(node)

    def add_line(self, line):
        self.lines.append(line)

    def remove_line(self, line):
        self.lines.remove(line)
