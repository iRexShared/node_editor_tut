from PySide2.QtWidgets import *
from PySide2.QtCore import *
from PySide2.QtGui import *
from node_graphics_view import QDMGraphicsView
from node_scene import Scene
from node_node import Node


class NodeEditorWindow(QWidget):
    """ Class which initializes the Node Editor Application"""
    def __init__(self, parent=None):
        super(NodeEditorWindow, self).__init__(parent)

        self.main_ly = None
        self.graphics_scene = None
        self.view = None
        self.scene = None

        self.init_ui()

    def init_ui(self):

        self.setGeometry(200, 200, 800, 600)
        self.main_ly = QHBoxLayout()
        self.setLayout(self.main_ly)
        self.main_ly.setContentsMargins(0, 0, 0, 0)
        self.setWindowFlags(Qt.WindowStaysOnTopHint)

        # create scene
        self.scene = Scene()
        self.graphics_scene = self.scene.graphics_scene

        node = Node(self.scene, 'Node')

        # create view
        self.view = QDMGraphicsView(self.scene.graphics_scene, self)
        #self.view.setScene(self.graphics_scene)
        self.main_ly.addWidget(self.view)

        self.setWindowTitle('Mr.T Editor')

        self.show()

        #self.add_debug_content()

    def add_debug_content(self):
        """Adding graphics items for debug purposes """
        dark_grey_brush = QBrush(Qt.darkGray)
        outline_pen = QPen(Qt.black)
        outline_pen.setWidth(2)

        # Add rectangular shape to graphics scene
        item = self.graphics_scene.addRect(-100, -100, 80, 100, outline_pen,  dark_grey_brush)
        item.setFlags(QGraphicsItem.ItemIsSelectable | QGraphicsItem.ItemIsMovable)
        item.setFlag(QGraphicsItem.ItemIsSelectable, False) # remove outline

        # Add simple texs to graphics scene
        text = self.graphics_scene.addText('Node Name', QFont('Ubuntu'))
        text.setFlags(QGraphicsItem.ItemIsSelectable | QGraphicsItem.ItemIsMovable)
        text.setFlag(QGraphicsItem.ItemIsSelectable, False)  # remove outline
        text.setDefaultTextColor(QColor.fromRgbF(1.0, 1.0, 1.0))


        #line = self.graphics_scene.addLine(-200, -200, 400, -100, outline_pen )
