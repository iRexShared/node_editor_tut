from PySide2.QtWidgets import *
from PySide2.QtCore import *
from PySide2.QtGui import *


class QDMGraphicsScene(QGraphicsScene):
    """This class defines graphics scene: greed appearance, background, grid-line colors/width etc"""
    def __init__(self, scene, parent=None):
        super(QDMGraphicsScene, self).__init__()

        self.scene = scene
        # scene settings
        self.grid = 10
        self.grid_over = 80

        self.background_color = QColor('#393939')
        self._bright_color = QColor('#2f2f2f')
        self._dark_color = QColor('#292929')

        self._bright_pen = QPen(self._bright_color)
        self._bright_pen.setWidth(1)
        self._dark_pen = QPen(self._dark_color)
        self._dark_pen.setWidth(2)

        self.scene_width = 70000
        self.scene_height = 70000

        self.setBackgroundBrush(self.background_color)

    def set_graphics_scene(self, width, height):
        self.setSceneRect(-width // 2, -height // 2, width, height)

    def drawBackground(self, painter, rect):
        """Draw scene grid with lines."""

        #draw all lines
        painter.fillRect(rect, self.background_color )

        # Bright
        left = int(rect.left()) - int((rect.left()) % self.grid)
        top = int(rect.top()) - int((rect.top()) % self.grid)
        right = int(rect.right())
        bottom = int(rect.bottom())
        lines_bright = []
        for x in range(left, right, self.grid):
            lines_bright.append(QLine(x, top, x, bottom))
        for y in range(top, bottom, self.grid):
            lines_bright.append(QLine(left, y, right, y))

        # Dark
        left = int(rect.left()) - int((rect.left()) % self.grid_over)
        top = int(rect.top()) - int((rect.top()) % self.grid_over)
        right = int(rect.right())
        bottom = int(rect.bottom())
        lines_dark = []
        for x in range(left, right, self.grid_over):
            lines_dark.append(QLine(x, top, x, bottom))
        for y in range(top, bottom, self.grid_over):
            lines_dark.append(QLine(left, y, right, y))

        painter.setPen(QPen(self._bright_pen))
        painter.drawLines(lines_bright)
        painter.setPen(QPen(self._dark_pen))
        painter.drawLines(lines_dark)

