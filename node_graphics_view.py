from PySide2.QtWidgets import *
from PySide2.QtCore import *
from PySide2.QtGui import *


class QDMGraphicsView(QGraphicsView):
    """Graphics view class. Here we define graphics view - scene navigation etc"""
    def __init__(self, graphics_scene, parent=None):
        super(QDMGraphicsView, self).__init__(parent)
        self.graphicsScene = graphics_scene

        self.init_ui()

        self.setScene(self.graphicsScene)

        self.zoom_in_factor = 1.25
        self.zoom_clamp = False
        self.zoom = 10
        self.zoom_step = 1
        self.zoom_range = [0, 10]

    def init_ui(self):
        self.setRenderHints(QPainter.Antialiasing | QPainter.HighQualityAntialiasing | QPainter.TextAntialiasing |
                            QPainter.SmoothPixmapTransform)

        self.setViewportUpdateMode(QGraphicsView.FullViewportUpdate)

        self.setHorizontalScrollBarPolicy(Qt.ScrollBarAlwaysOff)
        self.setVerticalScrollBarPolicy(Qt.ScrollBarAlwaysOff)
        self.setTransformationAnchor(QGraphicsView.AnchorUnderMouse)

    def mousePressEvent(self, event):
        if event.button() == Qt.MiddleButton:
            self.middle_mouse_button_press(event)
        elif event.button() == Qt.LeftButton:
            self.right_mouse_button_press(event)
        elif event.button() == Qt.RightButton:
            self.right_mouse_button_press(event)
        else:
            super(QDMGraphicsView, self).mousePressEvent(event)

    def mouseReleaseEvent(self, event):
        if event.button() == Qt.MiddleButton:
            self.middle_mouse_button_release(event)
        elif event.button() == Qt.LeftButton:
            self.left_mouse_button_release(event)
        elif event.button() == Qt.RightButton:
            self.right_mouse_button_release(event)
        else:
            super(QDMGraphicsView, self).mouseReleaseEvent(event)

    def middle_mouse_button_press(self, event):
        release_vent = QMouseEvent(QEvent.MouseButtonRelease, event.localPos(), event.screenPos(), Qt.MiddleButton,
                                   Qt.LeftButton, event.modifiers())
        super(QDMGraphicsView, self).mouseReleaseEvent(release_vent)
        self.setDragMode(QGraphicsView.ScrollHandDrag)
        #self.setCursor(Qt.CrossCursor)
        fake_event = QMouseEvent(event.type(), event.localPos(), event.screenPos(),
                                 Qt.LeftButton, event.buttons() | Qt.LeftButton, event.modifiers())
        super(QDMGraphicsView, self).mousePressEvent(fake_event)

    def middle_mouse_button_release(self, event):

        fake_event = QMouseEvent(event.type(), event.localPos(), event.screenPos(),
                                Qt.LeftButton, event.buttons() & ~Qt.LeftButton, event.modifiers())
        super(QDMGraphicsView, self).mouseReleaseEvent(fake_event)
        self.setDragMode(QGraphicsView.NoDrag)
        #self.setCursor(Qt.ArrowCursor)

    def left_mouse_button_press(self, event):
        return super(QDMGraphicsView, self).mousePressEvent(event)

    def left_mouse_button_release(self, event):
        return super(QDMGraphicsView, self).mouseReleaseEvent(event)

    def right_mouse_button_press(self, event):
        return super(QDMGraphicsView, self).mousePressEvent(event)

    def right_mouse_button_release(self, event):
        return super(QDMGraphicsView, self).mouseReleaseEvent(event)

    def wheelEvent(self, event):
        # calculate zoom Factor
        zoom_out_factor = 1 / self.zoom_in_factor

        # calculate zoom
        if event.angleDelta().y() > 0:
            zoom_factor = self.zoom_in_factor
            self.zoom += self.zoom_step
        else:
            zoom_factor = zoom_out_factor
            self.zoom -= self.zoom_step

        # set scene scale
        self.scale(zoom_factor, zoom_factor)

    def keyPressEvent(self, event):
        """Alt key press implementation"""
        key_press_event = QKeyEvent(event.type(), 0, event.modifiers(), 'Alt')
        if key_press_event.modifiers() == Qt.AltModifier:
            self.setDragMode(QGraphicsView.ScrollHandDrag)

        super(QDMGraphicsView, self).keyPressEvent(key_press_event)

    def keyReleaseEvent(self, event):
        """Alt key release implementation"""
        key_release_event = QKeyEvent(event.type(), 1, event.modifiers(), 'Alt')
        self.setDragMode(QGraphicsView.NoDrag)

        super(QDMGraphicsView, self).keyReleaseEvent(key_release_event)